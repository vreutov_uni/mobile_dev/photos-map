package com.example.mapwithmarker;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.mapwithmarker.contracts.MarkerContract;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by VReutov on 02.03.2018.
 *
 */

public class LocationService extends Service {
    final String LOG_TAG = "LocationService";
    private FusedLocationProviderClient mFusedLocationProviderClient;
    ExecutorService es;

    public void onCreate() {
        super.onCreate();
        initializeLocationClient();

        es = Executors.newFixedThreadPool(1);
        es.execute(new LocationProvider(this));

        Log.d(LOG_TAG, "Service created");
    }

    void initializeLocationClient() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "Service started with intent");
        return START_STICKY;
}

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "Service destroyed");
    }

    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return null;
    }

    class LocationProvider implements Runnable {
        private long mLocationGetTimeout;
        BroadcastReceiver mBroadcastReceiver;
        List<LatLng> mVisitedMarkers;
        Context mContext;
        LatLng mLastLocation;
        LatLng mLastScannedLocation;

        LocationProvider(Context context) {
            mContext = context;
            mLocationGetTimeout = Constants.LOCATION_DEFAULT_TIMEOUT;
            mVisitedMarkers = new Vector<>();
            registerTimeoutReceiver();
            Log.d(LOG_TAG, "LocationProvider created");
        }

        public void run() {
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    getDeviceLocation();
                    TimeUnit.MILLISECONDS.sleep(mLocationGetTimeout);
                } catch (InterruptedException ignored) { }

            }
        }

        private LatLng getLatLng(Location location) {
            return new LatLng(location.getLatitude(), location.getLongitude());
        }

        private void getDeviceLocation() {
            try {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {

                        if (task.isSuccessful()) {
                            final Location currentLocation = task.getResult();
                            final LatLng currentPosition = getLatLng(currentLocation);

                            if (mLastLocation == null || !Utils.equals(mLastLocation, currentPosition))
                            {
                                Intent intent = new Intent(Constants.LOCATION_UPDATE_ACTION);
                                intent.putExtra(Constants.LOCATION_KEY, currentLocation);
                                sendBroadcast(intent);

                                Log.d(LOG_TAG, "Location updated");

                                if (mLastScannedLocation == null
                                        || Utils.getDistanceBetween(mLastScannedLocation, currentPosition)
                                            >= Constants.LOCATION_NOTIFICATION_DISTANCE / 3)
                                {
                                    mLastScannedLocation = currentPosition;

                                    new Thread(new Runnable() {
                                        public void run() {
                                            checkNearestMarkers(currentPosition);
                                        }
                                    }).start();
                                }
                            }


                        }
                    }
                });
            } catch (SecurityException e)  {
                Log.e("Exception: %s", e.getMessage());
            }
        }

        private void checkNearestMarkers(LatLng position) {
            Realm realm = Realm.getDefaultInstance();

            RealmResults<MarkerContract> markers = realm.where(MarkerContract.class).findAll();
            List<LatLng> newVisited = new Vector<>();
            List<LatLng> nearby = new Vector<>();

            for (MarkerContract marker: markers) {
                if (Utils.getDistanceBetween(marker.getPosition(), position) < Constants.LOCATION_NOTIFICATION_DISTANCE) {
                    nearby.add(marker.getPosition());

                    if (marker.getImages().size() > 0)
                    {
                        boolean isVisited = false;

                        for (LatLng pos : mVisitedMarkers) {
                            if (Utils.equals(pos, marker.getPosition()))
                            {
                                isVisited = true;
                                break;
                            }
                        }

                        if (!isVisited)
                        {
                            newVisited.add(marker.getPosition());
                        }
                    }
                }
            }

            realm.close();

            mVisitedMarkers = nearby;

            if (newVisited.size() > 0) {
                Intent resultIntent = new Intent(mContext, MapsMarkerActivity.class);
                PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(mContext)
                                .setSmallIcon(android.R.drawable.ic_menu_compass)
                                .setContentTitle("Маркер недалеко от Вас!")
                                .setContentText(String.format("Новых маркеров: %d", newVisited.size()))
                                .setContentIntent(resultPendingIntent)
                                .setAutoCancel(true);

                Notification notification = builder.build();

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                assert notificationManager != null;
                notificationManager.notify(1, notification);
            }

            Log.d(LOG_TAG, "Location scanned");
        }

        void registerTimeoutReceiver() {
            mBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    mLocationGetTimeout = intent.getLongExtra(
                            Constants.LOCATION_TIMEOUT_KEY,
                            Constants.LOCATION_DEFAULT_TIMEOUT);
                }
            };

            IntentFilter intentFilter = new IntentFilter(Constants.LOCATION_SET_TIMEOUT_ACTION);
            registerReceiver(mBroadcastReceiver, intentFilter);
        }


        void stop() {
            unregisterReceiver(mBroadcastReceiver);
            Log.d(LOG_TAG, "LocationProvider stopped");
        }
    }
}
