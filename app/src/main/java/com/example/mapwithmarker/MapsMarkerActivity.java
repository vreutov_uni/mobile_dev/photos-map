package com.example.mapwithmarker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.mapwithmarker.contracts.MarkerContract;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;

import io.realm.Realm;
import io.realm.RealmResults;


public class MapsMarkerActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        OnMapClickListener,
        OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener
{
    private static class ServiceLocationSource implements LocationSource {
        private Context mContext;
        private Location mLastLocation;

        private OnLocationChangedListener mListener;
        private BroadcastReceiver br;
        private boolean mPaused;

        ServiceLocationSource(Context context) {
            mContext = context;
            registerLocationReceiver();
        }

        @Override
        public void activate(OnLocationChangedListener listener) {
            mListener = listener;
        }

        @Override
        public void deactivate() {
            mListener = null;
        }

        private void registerLocationReceiver() {
            br = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (mListener != null && !mPaused) {
                        Location location = intent.getParcelableExtra(Constants.LOCATION_KEY);

                        if (location != null)
                            mListener.onLocationChanged(location);
                    }
                }
            };

            IntentFilter intentFilter = new IntentFilter(Constants.LOCATION_UPDATE_ACTION);
            mContext.registerReceiver(br, intentFilter);
        }

        void onPause() {
            mPaused = true;

            Intent intent = new Intent(Constants.LOCATION_SET_TIMEOUT_ACTION);
            intent.putExtra(Constants.LOCATION_TIMEOUT_KEY, (long) 2000);
            mContext.sendBroadcast(intent);
        }

        void onResume() {
            mPaused = false;

            Intent intent = new Intent(Constants.LOCATION_SET_TIMEOUT_ACTION);
            intent.putExtra(Constants.LOCATION_TIMEOUT_KEY, Constants.LOCATION_DEFAULT_TIMEOUT);
            mContext.sendBroadcast(intent);
        }

        void onDestroy() {
            mContext.unregisterReceiver(br);
        }

        public LatLng getLastLocation() {
            return new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }
    }

    private ServiceLocationSource mLocationSource;

    private GoogleMap mMap;
    private boolean mLocationPermissionGranted;

    private Marker mLastMarker;
    private LatLng mLastMarkerPos;
    private CameraPosition mCameraPosition;

    private Realm mRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (savedInstanceState != null) {
            mCameraPosition = savedInstanceState.getParcelable(Constants.KEY_CAMERA);
            if (savedInstanceState.containsKey(Constants.KEY_LAST_MARKER))
                mLastMarkerPos = savedInstanceState.getParcelable(Constants.KEY_LAST_MARKER);
        }

        mLastMarker = null;

        getLocationPermission();

        if (mLocationPermissionGranted) {
            mLocationSource = new ServiceLocationSource(this);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Realm.init(this);

        mRealm = Realm.getDefaultInstance();

        startService(new Intent(this, LocationService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mLocationSource != null)
            mLocationSource.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mLocationSource != null)
            mLocationSource.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        removeLastIfEmpty();

        if (mLocationSource != null)
            mLocationSource.onDestroy();
    }

    private void createMarkers() {
        final RealmResults<MarkerContract> markers = mRealm.where(MarkerContract.class).findAll();

        for (MarkerContract marker : markers) {
            createNewMarker(marker.getPosition());
        }
    }

    private MarkerContract getLinkedMarker(Marker marker)
    {
        return MarkerContract.getFromLocation(mRealm, marker.getPosition());
    }

    private boolean HasImages(Marker marker)
    {
        MarkerContract linkedMarker = getLinkedMarker(marker);
        return linkedMarker.getImages().size() != 0;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.KEY_CAMERA, mMap.getCameraPosition());
        if (mLastMarker != null && !HasImages(mLastMarker))
            outState.putParcelable(Constants.KEY_LAST_MARKER, mLastMarker.getPosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        setupListeners();
        createMarkers();

        if (mLocationPermissionGranted) {
            mMap.setLocationSource(mLocationSource);
        }

        updateLocationUI();

        if (mCameraPosition != null)
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));

        if (mLastMarkerPos != null)
            createNewMarker(mLastMarkerPos).showInfoWindow();
    }

    private void setupListeners() {
        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private Marker createNewMarker(LatLng point)
    {
        mLastMarker = mMap.addMarker(new MarkerOptions()
                .position(point)
                .title("Add new photo..."));
        return mLastMarker;
    }

    private void removeLastIfEmpty() {
        if (mLastMarker != null && !HasImages(mLastMarker))
        {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    MarkerContract.getFromLocation(mRealm, mLastMarker.getPosition()).deleteFromRealm();
                }
            });

            mLastMarker.remove();
            mLastMarker = null;
        }
    }

    @Override
    public void onMapClick(final LatLng point) {
        removeLastIfEmpty();

        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                MarkerContract marker = mRealm.createObject(MarkerContract.class);
                marker.setPosition(point);
            }
        });

        createNewMarker(point).showInfoWindow();
        mMap.animateCamera(CameraUpdateFactory.newLatLng(point));
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        removeLastIfEmpty();

        if (marker.isInfoWindowShown())
            marker.hideInfoWindow();

        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(this, PhotosActivity.class);
        intent.putExtra(Constants.EXTRA_LOCATION, marker.getPosition());
        startActivity(intent);
    }
}
