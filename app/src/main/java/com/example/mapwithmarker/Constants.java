package com.example.mapwithmarker;

/**
 * Created by VReutov on 03.03.2018.

 * Some constants
 */

class Constants {
    private final static String PACKAGE_NAME = "com.example.mapwithmarker";

    static final String LOCATION_UPDATE_ACTION = PACKAGE_NAME + ".location_update";
    static final String LOCATION_SET_TIMEOUT_ACTION = PACKAGE_NAME + ".location_set_timeout";
    static final String EXTRA_LOCATION = PACKAGE_NAME + ".location";

    static final String LOCATION_KEY = "location_frequency";
    static final String LOCATION_TIMEOUT_KEY = "location_frequency";

    static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    static final String KEY_CAMERA = "camera";
    static final String KEY_LAST_MARKER = "last_marker";

    static final long LOCATION_DEFAULT_TIMEOUT = 600; // in milliseconds
    static final long LOCATION_NOTIFICATION_DISTANCE = 100; // in meters
}
