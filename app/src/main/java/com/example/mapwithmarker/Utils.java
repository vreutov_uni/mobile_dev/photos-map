package com.example.mapwithmarker;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by VReutov on 03.03.2018.
 */

public class Utils {
    static double getDistanceBetween(LatLng one, LatLng other) {
        float[] results = new float[1];

        Location.distanceBetween(
                one.latitude, one.longitude,
                other.latitude, other.longitude,
                results);

        return results[0];
    }

    static boolean equals(LatLng one, LatLng other) {
        return one.latitude == other.latitude
                && one.longitude == other.longitude;
    }
}
