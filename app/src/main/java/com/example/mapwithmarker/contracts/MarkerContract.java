package com.example.mapwithmarker.contracts;

import com.google.android.gms.maps.model.LatLng;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by VReutov on 02.03.2018.
 */

public class MarkerContract extends RealmObject {
    private double latitude;
    private double longitude;
    private RealmList<ImageContract> images;

    public void setPosition(LatLng position) {
        latitude = position.latitude;
        longitude = position.longitude;
    }

    public LatLng getPosition() {
        return new LatLng(latitude, longitude);
    }

    public RealmList<ImageContract> getImages() {
        return images;
    }

    public static MarkerContract getFromLocation(Realm realm, LatLng location) {
        return realm
                .where(MarkerContract.class)
                .equalTo("latitude", location.latitude)
                .and()
                .equalTo("longitude", location.longitude)
                .findFirst();
    }

    public void setImages(RealmList<ImageContract> images) {
        this.images = images;
    }
}
