package com.example.mapwithmarker.contracts;

import io.realm.RealmObject;

/**
 * Created by VReutov on 02.03.2018.
 */

public class ImageContract extends RealmObject {
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
